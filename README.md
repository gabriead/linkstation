## Quickstart

Start server either with gradlew bootRun on the terminal or open src/main/java/app/App and
hit the run button next to the main method. 
The results of the calculation will be printed to the console.
This application is dockerized and can be run as a container.

