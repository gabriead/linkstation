package Service;

import Domain.LinkStation;
import Domain.Position;
import javafx.util.Pair;
import org.junit.Test;

import static TestData.TestData.createLinkStations;
import static org.assertj.core.api.Assertions.assertThat;

public class DistanceResolverServiceTest {

    public static final int EXPONENT_TO_POWER_OF_TWO = 2;
    private DistanceResolverService distanceResolverService = new DistanceResolverService();

    @Test
    public void computeDistanceToLinkStationFromPointInOriginCorrectly() {

        final int xPosOfPointInOrigin = 0;
        final int yPosOfPointInOrigin = 0;

        final int xPosLinkStation1 = 20;
        final int yPosLinkStation1 = 20;

        final int xPosLinkStation2 = 10;
        final int yPosLinkStation2 = 0;

        final int xPosLinkStation3 = 0;
        final int yPosLinkStation3 = 0;

        final Position positionInPointOfOrigin = new Position(xPosOfPointInOrigin, yPosOfPointInOrigin);
        final LinkStation linkStation1 = new LinkStation(xPosLinkStation1, yPosLinkStation1, 0);
        final LinkStation linkStation2 = new LinkStation(xPosLinkStation2, yPosLinkStation2, 0);
        final LinkStation linkStation3 = new LinkStation(xPosLinkStation3, yPosLinkStation3, 0);


        final double actualDistanceToLinkStation1 = distanceResolverService.computeDistanceToLinkStation(positionInPointOfOrigin, linkStation1);
        final double expectDistanceToLinkStation1 = Math.sqrt((Math.pow(20 - xPosOfPointInOrigin, 2) + (Math.pow(20 - yPosOfPointInOrigin, 2))));
        assertThat(actualDistanceToLinkStation1).isEqualTo(expectDistanceToLinkStation1);

        final double actualDistanceToLinkStation2 = distanceResolverService.computeDistanceToLinkStation(positionInPointOfOrigin, linkStation2);
        final double expectDistanceToLinkStation2 = Math.sqrt((Math.pow(10 - xPosOfPointInOrigin, 2) + (Math.pow(0 - xPosOfPointInOrigin, 2))));
        assertThat(actualDistanceToLinkStation2).isEqualTo(expectDistanceToLinkStation2);

        final double actualDistanceToLinkStation3 = distanceResolverService.computeDistanceToLinkStation(positionInPointOfOrigin, linkStation3);
        final double expectDistanceToLinkStation3 = Math.sqrt((Math.pow(0 - xPosOfPointInOrigin, 2) + (Math.pow(0 - xPosOfPointInOrigin, 2))));
        assertThat(actualDistanceToLinkStation3).isEqualTo(expectDistanceToLinkStation3);

    }

    @Test
    public void computeDistanceToLinkStationsFromRandomPointCorrectly() {

        final int xPosOfPoint = 0;
        final int yPosOfPoint = 0;

        final int xPosLinkStation1 = 20;
        final int yPosLinkStation1 = 20;

        final int xPosLinkStation2 = 10;
        final int yPosLinkStation2 = 0;

        final Position positionPoint = new Position(xPosOfPoint, yPosOfPoint);
        final LinkStation linkStation1 = new LinkStation(xPosLinkStation1, yPosLinkStation1, 0);
        final LinkStation linkStation2 = new LinkStation(xPosLinkStation2, yPosLinkStation2, 0);

        final double actualDistanceToLinkStation1 = distanceResolverService.computeDistanceToLinkStation(positionPoint, linkStation1);
        final double expectDistanceToLinkStation1 = Math.sqrt((Math.pow(xPosLinkStation1 - xPosOfPoint, EXPONENT_TO_POWER_OF_TWO) + (Math.pow(xPosLinkStation1 - yPosOfPoint, EXPONENT_TO_POWER_OF_TWO))));
        assertThat(actualDistanceToLinkStation1).isEqualTo(expectDistanceToLinkStation1);

        final double actualDistanceToLinkStation2 = distanceResolverService.computeDistanceToLinkStation(positionPoint, linkStation2);
        final double expectDistanceToLinkStation2 = Math.sqrt((Math.pow(xPosLinkStation2 - xPosOfPoint, EXPONENT_TO_POWER_OF_TWO) + (Math.pow(yPosOfPoint - yPosLinkStation2, EXPONENT_TO_POWER_OF_TWO))));
        assertThat(actualDistanceToLinkStation2).isEqualTo(expectDistanceToLinkStation2);
    }

    @Test
    public void computeMostSuitableLinkStationFromPointInOriginCorrectly() {

        final int xPosOfPoint = 0;
        final int yPosOfPoint = 0;
        final Position positionPoint = new Position(xPosOfPoint, yPosOfPoint);

        final Pair<LinkStation, Double> actualMostSuitableLinkStation = distanceResolverService.computeMostSuitableLinkStation(positionPoint, createLinkStations());
        final LinkStation expectedlinkStation = new LinkStation(0, 0, 100);

        assertThat(actualMostSuitableLinkStation.getKey().getxPosition()).isEqualTo(expectedlinkStation.getxPosition());
        assertThat(actualMostSuitableLinkStation.getKey().getyPosition()).isEqualTo(expectedlinkStation.getyPosition());
        assertThat(actualMostSuitableLinkStation.getValue()).isEqualTo(expectedlinkStation.getReach());

    }

    @Test
    public void computeMostSuitableLinkStationFromPointCorrectly() {

        final int xPosOfPoint = 15;
        final int yPosOfPoint = 10;
        final Position positionPoint = new Position(xPosOfPoint, yPosOfPoint);

        final Pair<LinkStation, Double> actualMostSuitableLinkStation = distanceResolverService.computeMostSuitableLinkStation(positionPoint, createLinkStations());

        final int reachOfLinkStation = 12;
        final int xPosLinkStation = 10;
        final int yPosLinkStation = 0;
        final LinkStation expectedLinkStation = new LinkStation(xPosLinkStation, yPosLinkStation, reachOfLinkStation);

        final double xDiff = xPosOfPoint - xPosLinkStation;
        final double yDiff = yPosOfPoint - yPosLinkStation;
        final double distancePointToLinkStation = Math.sqrt((Math.pow(xDiff, EXPONENT_TO_POWER_OF_TWO)) + (Math.pow(yDiff, EXPONENT_TO_POWER_OF_TWO)));
        final double expectedPower = Math.pow(reachOfLinkStation - distancePointToLinkStation,2);
        final Pair<LinkStation, Double> expectedMostStuitableLinkStation = new Pair<>(expectedLinkStation, expectedPower);

        assertThat(actualMostSuitableLinkStation.getKey().getxPosition()).isEqualTo(expectedMostStuitableLinkStation.getKey().getxPosition());
        assertThat(actualMostSuitableLinkStation.getKey().getyPosition()).isEqualTo(expectedMostStuitableLinkStation.getKey().getyPosition());
        assertThat(actualMostSuitableLinkStation.getValue()).isEqualTo(expectedMostStuitableLinkStation.getValue());
    }

    @Test
    public void computeMostSuitableLinkStationFromOtherPointCorrectly() {

        final int xPosOfPoint = 18;
        final int yPosOfPoint = 18;
        final Position positionPoint = new Position(xPosOfPoint, yPosOfPoint);

        final Pair<LinkStation, Double> actualMostSuitableLinkStation = distanceResolverService.computeMostSuitableLinkStation(positionPoint, createLinkStations());

        final int reachOfLinkStation = 5;
        final int xPosLinkStation = 20;
        final int yPosLinkStation = 20;
        final LinkStation expectedLinkStation = new LinkStation(xPosLinkStation, yPosLinkStation, reachOfLinkStation);

        final double xDiff = xPosOfPoint - xPosLinkStation;
        final double yDiff = yPosOfPoint - yPosLinkStation;
        final double distancePointToLinkStation = Math.sqrt((Math.pow(xDiff, EXPONENT_TO_POWER_OF_TWO)) + (Math.pow(yDiff, EXPONENT_TO_POWER_OF_TWO)));
        final double expectedPower = Math.pow(reachOfLinkStation - distancePointToLinkStation,2);
        final Pair<LinkStation, Double> expectedMostStuitableLinkStation = new Pair<>(expectedLinkStation, expectedPower);

        assertThat(actualMostSuitableLinkStation.getKey().getxPosition()).isEqualTo(expectedMostStuitableLinkStation.getKey().getxPosition());
        assertThat(actualMostSuitableLinkStation.getKey().getyPosition()).isEqualTo(expectedMostStuitableLinkStation.getKey().getyPosition());
        assertThat(actualMostSuitableLinkStation.getValue()).isEqualTo(expectedMostStuitableLinkStation.getValue());
    }

}