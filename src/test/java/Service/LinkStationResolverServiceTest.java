package Service;

import Domain.LinkStation;
import Domain.Position;
import Service.CalculationTools.UnitTestCalculationTools;
import javafx.util.Pair;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static TestData.TestData.createLinkStations;
import static TestData.TestData.createPositions;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class LinkStationResolverServiceTest {


    @Mock
    private DistanceResolverService distanceResolverService;

    private LinkStationResolverService linkStationResolverService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        linkStationResolverService = new LinkStationResolverService(distanceResolverService);
    }


    @Test
    public void computeLinkStationsForFirstPhonePositionCorrectly() {

        final List<Position> positions = createPositions();
        final List<LinkStation> linkStations = createLinkStations();
        final LinkStation expectedLinkStation = new LinkStation(0, 0, 10);


        final double distanceFromPointToLinkStation = UnitTestCalculationTools.distanceFromPointToLinkStation(positions.get(0).getxPosition(),
                positions.get(0).getyPosition(), linkStations.get(0));

        final Pair<LinkStation, Double> mostSuitableLinkStation =
                UnitTestCalculationTools.createMostSuitableLinkStationFrom(expectedLinkStation, distanceFromPointToLinkStation);

        when(distanceResolverService.computeMostSuitableLinkStation(positions.get(0), linkStations)).
                thenReturn(mostSuitableLinkStation);

        final List<String> actualResultMessages =
                linkStationResolverService.computeLinkStationsForPhonePositions(positions, linkStations);

        String expectedMessage = "Best link station for point [0,0] is  xPos: 0, yPos: 0  with power 100.0";
        assertThat(actualResultMessages.get(0)).isEqualTo(expectedMessage);
    }

    @Test
    public void computeLinkStationsForSecondPhonePositionCorrectly() {

        final List<Position> positions = createPositions();
        final List<LinkStation> linkStations = createLinkStations();

        when(distanceResolverService.computeMostSuitableLinkStation(positions.get(1), linkStations)).
                thenReturn(null);

        final List<String> actualResultMessages =
                linkStationResolverService.computeLinkStationsForPhonePositions(positions, linkStations);

        String expectedMessage = "No link station within reach for point [100,100]";
        assertThat(actualResultMessages.get(1)).isEqualTo(expectedMessage);
    }

    @Test
    public void computeLinkStationsForThirdPhonePositionCorrectly() {

        final List<Position> positions = createPositions();
        final List<LinkStation> linkStations = createLinkStations();
        final LinkStation expectedLinkStation = new LinkStation(10, 0, 12);

        final double distanceFromPointToLinkStation = UnitTestCalculationTools.distanceFromPointToLinkStation(positions.get(2).getxPosition(),
                positions.get(2).getyPosition(), linkStations.get(2));

        final Pair<LinkStation, Double> mostSuitableLinkStation =
                UnitTestCalculationTools.createMostSuitableLinkStationFrom(expectedLinkStation, distanceFromPointToLinkStation);

        when(distanceResolverService.computeMostSuitableLinkStation(positions.get(2), linkStations)).
                thenReturn(mostSuitableLinkStation);

        final List<String> actualResultMessages =
                linkStationResolverService.computeLinkStationsForPhonePositions(positions, linkStations);

        String expectedMessage = "Best link station for point [15,10] is  xPos: 10, yPos: 0  with power 0.6718427000252355";
        assertThat(actualResultMessages.get(2)).isEqualTo(expectedMessage);
    }

    @Test
    public void computeLinkStationsForFourthPhonePositionCorrectly() {

        final List<Position> positions = createPositions();
        final List<LinkStation> linkStations = createLinkStations();
        final LinkStation expectedLinkStation = new LinkStation(20, 20, 5);

        final double distanceFromPointToLinkStation = UnitTestCalculationTools.distanceFromPointToLinkStation(positions.get(3).getxPosition(),
                positions.get(3).getyPosition(), linkStations.get(1));

        final Pair<LinkStation, Double> mostSuitableLinkStation =
                UnitTestCalculationTools.createMostSuitableLinkStationFrom(expectedLinkStation, distanceFromPointToLinkStation);

        when(distanceResolverService.computeMostSuitableLinkStation(positions.get(3), linkStations)).
                thenReturn(mostSuitableLinkStation);

        final List<String> actualResultMessages =
                linkStationResolverService.computeLinkStationsForPhonePositions(positions, linkStations);

        String expectedMessage = "Best link station for point [18,18] is  xPos: 20, yPos: 20  with power 4.715728752538098";
        assertThat(actualResultMessages.get(3)).isEqualTo(expectedMessage);
    }
}