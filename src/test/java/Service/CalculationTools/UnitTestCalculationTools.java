package Service.CalculationTools;

import Domain.LinkStation;
import javafx.util.Pair;

/**
 * Class implements calculation methods that are use within unit tests
 */
public final class UnitTestCalculationTools {

    private static final int EXPONENT_TO_POWER_OF_TWO = 2;

    /**
     * Method calculates the distance between a given position and a link station
     *
     * @param xPosOfPoint x-coordinate of point to calculate distance to link station from
     * @param yPosOfPoint y-coordinate of point to calculate distance to link station from
     * @param linkStation link station to calculate distance to
     * @return distance between point and link station
     */
    public static double distanceFromPointToLinkStation(int xPosOfPoint, int yPosOfPoint, LinkStation linkStation) {
        final double xDiff = xPosOfPoint - linkStation.getxPosition();
        final double yDiff = yPosOfPoint - linkStation.getyPosition();
        return Math.sqrt((Math.pow(xDiff, EXPONENT_TO_POWER_OF_TWO)) + (Math.pow(yDiff, EXPONENT_TO_POWER_OF_TWO)));
    }


    /**
     * Method creates the most suitable link station
     *
     * @param expectedLinkStation        link station that is the expected perfect match
     * @param distancePointToLinkStation distance between link station and a point
     * @return a key-value pair of link station and it's power
     */
    public static Pair<LinkStation, Double> createMostSuitableLinkStationFrom(LinkStation expectedLinkStation,
                                                                              double distancePointToLinkStation) {
        final double expectedPower = Math.pow(expectedLinkStation.getReach() - distancePointToLinkStation, 2);
        return new Pair<>(expectedLinkStation, expectedPower);
    }

}
