package TestData;

import Domain.LinkStation;
import Domain.Position;

import java.util.Arrays;
import java.util.List;

public final class TestData {


    public static List<LinkStation> createLinkStations() {
        final LinkStation linkStation1 = new LinkStation(0, 0, 10);
        final LinkStation linkStation2 = new LinkStation(20, 20, 5);
        final LinkStation linkStation3 = new LinkStation(10, 0, 12);
        return Arrays.asList(linkStation1, linkStation2, linkStation3);
    }

    public static List<Position> createPositions() {
        final Position position1 = new Position(0, 0);
        final Position position2 = new Position(100, 100);
        final Position position3 = new Position(15, 10);
        final Position position4 = new Position(18, 18);
        return Arrays.asList(position1, position2, position3, position4);
    }
}
