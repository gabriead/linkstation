package Domain;

public class LinkStation extends Position {

    private int reach;


    public LinkStation(int positionX, int positionY, int reach) {
        super(positionX, positionY);
        this.reach = reach;
    }

    public int getReach() {
        return reach;
    }


}
