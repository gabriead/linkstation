package SystemConstants;

public final class PrintMessageConstants {
    public static final String NO_LINK_STATION_WITHIN_REACH_FOR_POINT_MESSAGE = "No link station within reach for point ";
    public static final String BEST_LINK_STATION_FOR_POINT_MESSAGE = "Best link station for point ";
    public static final int MINIMUM_AMOUNT_POWER = 0;
    public static final String COMMA = ",";
    public static final String X_POS = " is " + " xPos: ";
    public static final String Y_POS = "," + " yPos: ";
    public static final String BRAKET_LEFT_ORIENTATION = "[";
    public static final String BRAKET_RIGHT_ORIENTATION = "]";
}
