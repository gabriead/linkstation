package app;

import Service.DistanceResolverService;
import Service.LinkStationResolverService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

import static TestData.TestData.createLinkStations;
import static TestData.TestData.createPositions;


/**
 * Main class of the application.
 */

@SpringBootApplication
public class App {

    private static DistanceResolverService distanceResolverService = new DistanceResolverService();
    private static LinkStationResolverService linkStationResolverService = new LinkStationResolverService(distanceResolverService);

    public static void main(String[] args) {

        SpringApplication.run(App.class, args);

        System.out.println("-------Begin of calculation---------");
        final List<String> messages = linkStationResolverService.computeLinkStationsForPhonePositions(createPositions(), createLinkStations());
        linkStationResolverService.printResultMessages(messages);
    }

}
