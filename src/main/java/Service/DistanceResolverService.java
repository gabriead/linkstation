package Service;

import Domain.LinkStation;
import Domain.Position;
import javafx.util.Pair;

import java.util.List;

/**
 * Class computes the most suitable link station from a collection of link stations
 */
public class DistanceResolverService {


    /**
     * @param phonePosition phone position
     * @param linkStations  collection of link stations
     * @return suitable link station and the coordinates of the phone it was computed for or null, if no
     * link station fullfills the requirements
     */
    public Pair<LinkStation, Double> computeMostSuitableLinkStation(Position phonePosition, List<LinkStation> linkStations) {
        Pair<LinkStation, Double> mostSuitableLinkStation = null;
        double greatestPower = 0;

        for (LinkStation currentLinkStation : linkStations) {
            final double distanceToLinkStation = computeDistanceToLinkStation(phonePosition, currentLinkStation);
            final double reach = currentLinkStation.getReach();
            final double currentPower = computePower(reach, distanceToLinkStation);

            if (currentPower > greatestPower) {
                greatestPower = currentPower;
                mostSuitableLinkStation = new Pair<>(currentLinkStation, currentPower);
            }
        }

        return mostSuitableLinkStation != null ? mostSuitableLinkStation : null;
    }

    private double computePower(double reach, double distanceFromLinkStation) {

        if (distanceFromLinkStation > reach) {
            return 0;
        }

        return Math.pow((reach - distanceFromLinkStation), 2);
    }

    public double computeDistanceToLinkStation(Position currentPhonePosition, LinkStation linkStationPosition) {

        final int phoneXPosition = currentPhonePosition.getxPosition();
        final int phoneYPosition = currentPhonePosition.getyPosition();

        final int linkStationXPosition = linkStationPosition.getxPosition();
        final int linkStationYPosition = linkStationPosition.getyPosition();

        final double absoluteSquaredDistanceXPosition = squaredDifferenceBetweenTwoCoordinates(phoneXPosition, linkStationXPosition);
        final double absoluteSquaredDistanceYPosition = squaredDifferenceBetweenTwoCoordinates(phoneYPosition, linkStationYPosition);

        return Math.sqrt(absoluteSquaredDistanceXPosition + absoluteSquaredDistanceYPosition);

    }

    private double squaredDifferenceBetweenTwoCoordinates(int coordinate1, int coordinate2) {
        return Math.pow((coordinate2 - coordinate1), 2);
    }
}
