package Service;

import Domain.LinkStation;
import Domain.Position;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;

import static SystemConstants.PrintMessageConstants.*;

/**
 * Service computes the most suitable link station for a given position.
 */

public class LinkStationResolverService {

    private DistanceResolverService distanceResolverService;

    public LinkStationResolverService(DistanceResolverService distanceResolverService) {
        this.distanceResolverService = distanceResolverService;
    }

    /**
     * Methode computes the most suitable link station for a given phone position and prints it to the console
     *
     * @param phonePositions collection of phone positions
     * @param linkStations   collection of link stations
     */
    public List<String> computeLinkStationsForPhonePositions(List<Position> phonePositions, List<LinkStation> linkStations) {
        List<String> resultMessages = new ArrayList<>();

        for (Position currentPhonePosition : phonePositions) {
            final Pair<LinkStation, Double> mostSuitableLinkStation =
                    distanceResolverService.computeMostSuitableLinkStation(currentPhonePosition, linkStations);
            resultMessages.add(generateResultMessage(mostSuitableLinkStation, currentPhonePosition));
        }

        return resultMessages;
    }

    /**
     * Method prints the results to the console
     *
     * @param messages that are printed to the console
     */
    public void printResultMessages(List<String> messages) {
        for (String result : messages) {
            System.out.println(result);
        }
    }

    private String generateResultMessage(Pair<LinkStation, Double> mostSuitableLinkStation, Position position) {

        if (mostSuitableLinkStation == null) {
            return (buildLinkStationNotExistsMessage(
                    position.getxPosition(),
                    position.getyPosition()));
        }

        final LinkStation linkStation = mostSuitableLinkStation.getKey();
        final int xPosition = linkStation.getxPosition();
        final int yPosition = linkStation.getyPosition();

        return (buildLinkStationExistsMessage(position, mostSuitableLinkStation.getValue(), xPosition, yPosition));

    }

    private String buildLinkStationExistsMessage(Position position1, Double power, int xPosition, int yPosition) {
        StringBuilder builder = new StringBuilder();
        builder.append(BEST_LINK_STATION_FOR_POINT_MESSAGE);
        builder.append(BRAKET_LEFT_ORIENTATION);
        builder.append(position1.getxPosition());
        builder.append(COMMA);
        builder.append(position1.getyPosition());
        builder.append(BRAKET_RIGHT_ORIENTATION);
        builder.append(X_POS);
        builder.append(xPosition);
        builder.append(Y_POS);
        builder.append(yPosition);
        builder.append("  with power ");
        builder.append(power);

        return builder.toString();
    }

    private String buildLinkStationNotExistsMessage(int xPosition, int yPosition) {
        StringBuilder builder = new StringBuilder();
        builder.append(NO_LINK_STATION_WITHIN_REACH_FOR_POINT_MESSAGE);
        builder.append(BRAKET_LEFT_ORIENTATION);
        builder.append(xPosition);
        builder.append(COMMA);
        builder.append(yPosition);
        builder.append(BRAKET_RIGHT_ORIENTATION);

        return builder.toString();
    }

}
