package Rest;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * Rest controller that handles the requests.
 */

@SpringBootApplication
@RestController
public class LinkStationCalculationController {


    @GetMapping("/")
    public String startComputation() {
        //TODO: implement REST-Controller if needed
        throw new NotImplementedException();
    }
}
